
package jScanner;

/**
 * This Class represents the Class- and Relative Code
 * that corresponds to a found Token during the scanning
 * process. There is one KCRCType object for each Token
 * found in a Sourcefile.
 * It consists only of standard getter and setter methods as well as a toString Method
 *
 * @author (Achim Guldner)
 * @version (1.0)
 */
public class KCRCType {
    /**
     * Stores the ClassCode of the found Token.
     */
    private short kc;
    /**
     * Stores the RelativeCode of the found Token.
     */
    private short rc;
    /**
     * the constructor.
     */
    public KCRCType() { }

    /**
     * @return the kc
     */
    public final int getKc() {
        return kc;
    }

    /**
     * @param kc the kc to set
     */
    public final void setKc(final short kc) {
        this.kc = kc;
    }

    /**
     * @return the rc
     */
    public final int getRc() {
        return rc;
    }

    /**
     * @param rc the rc to set
     */
    public final void setRc(final short rc) {
        this.rc = rc;
    }

    /**
     * @return a String representing the ClassCode and RelativeCode
     */
    public final String toString() {
        return ("CC: " + this.kc + " \t| RC: " + this.rc);
    }
}
