package jScanner;

import POCO_generated.ScanTabs;
import red_black_tree.Item;
import red_black_tree.Tree;
import POCO_generated.SysInfo;

/**
 * This Class incorporates the functionality of creating the parse tree.
 *
 * @author (Achim Guldner)
 * @version (1.0)
 */
public class ScanTreeBuilder {
    /**
     * With HASHON/HASHOFF the user may choose if the tree is divided into subtrees
     * that are managed by a hash-function.
     */
    private final boolean HASHON = SysInfo.HASHON;
    /**
     * With HASHON/HASHOFF the user may choose if the tree is divided into subtrees
     * that are managed by a hash-function.
     */
    private final boolean HASHOFF=!HASHON;
    /**
     * the hash is calculated using this value.
     */
    private final int maxIdHash = 224;
    /**
     * the array that holds the items that were inserted last.
     */
    private Item[] itemArray;
    /**
     * the lextree (used only if hashon=true).
     */
    private Tree tree;
    /**
     * the last found kc.
     */
    private int lastFoundKc = 0;
    /**
     * the number of tokens that were inserted into the tree.
     */
    protected int difTokenCount;
    /**
     * the array that holds the lextrees if hashon==true.
     */
    private Tree[] hashArray;

    /**
     * the constructor initializes the tree with predefined tokens (e.g. end, begin, or, mod, inline, etc.)
     */
    public ScanTreeBuilder() {
        itemArray = new Item[ScanTabs.maxKC + 1]; // BUG: Of by one. ( No +1 before )
        //initialize Tree with predefined tokens (e.g. end, begin, or, mod, inline, etc.)
        String token = new String("");
        if (HASHOFF) {
            this.tree = new Tree();
            for (int i = 0; i < ScanTabs.LexTab.length - 1; i++) {
                short tokenLength = ScanTabs.LexTab[i];
                for (byte b = 1; b <= tokenLength; b++){
                    token += (char) ScanTabs.LexTab[i + b];
                }
                Item item = new Item();
                KCRCType kcrc = new KCRCType();
                item.setKey(token.toUpperCase());
                kcrc.setKc(ScanTabs.LexTab[i + tokenLength + 1]);
                kcrc.setRc(ScanTabs.LexTab[i + tokenLength + 2]);
                item.setContent(kcrc);
                tree.insert(item);
                i += tokenLength + 2;
                token = "";
            }
        } else {
            hashArray = new Tree[maxIdHash];
            for (int i = 0; i < maxIdHash; i++) {
                hashArray[i] = new Tree();
            }
            for (int i = 0; i < ScanTabs.LexTab.length - 1; i++) {
                short tokenLength = ScanTabs.LexTab[i];
                for (byte b = 1; b <= tokenLength; b++){
                    token += (char) ScanTabs.LexTab[i + b];
                }
                int hashValue = hash(token);
                Item item = new Item();
                KCRCType kcrc = new KCRCType();
                item.setKey(token.toUpperCase());
                kcrc.setKc(ScanTabs.LexTab[i + tokenLength + 1]);
                kcrc.setRc(ScanTabs.LexTab[i + tokenLength + 2]);
                item.setContent(kcrc);
                hashArray[hashValue].insert(item);
                i += tokenLength + 2;
                token = "";
            }
        }
        difTokenCount = 0;
    }

    /**
     * looksup the key inside the tree.
     * if found it returns the according rc
     * otherwise it calls "newEntry" and inserts the new item into the tree
     * or if HASHON==true into the tree corresponding to the hash value.
     *
     * @param key the key
     * @param kc the kc
     * @return the generated rc
     */
    public final int enter(final String key, final int kc) {
        if (HASHOFF) {
            Item item = tree.search(key);
            if (item != null) {
                this.lastFoundKc = ((KCRCType) item.getContent()).getKc();
                return ((KCRCType) item.getContent()).getRc();
            } else {
                int rc = this.newEntry(key, kc);
                tree.insert(this.itemArray[kc]);
                this.lastFoundKc = kc;
                return (rc);
            }
        } else {
            int hashValue = hash(key);
            Item item = hashArray[hashValue].search(key);
            if (item != null) {
                this.lastFoundKc = ((KCRCType) item.getContent()).getKc();
                return ((KCRCType) item.getContent()).getRc();
            } else {
                int rc = this.newEntry(key, kc);
                hashArray[hashValue].insert(this.itemArray[kc]);
                this.lastFoundKc = kc;
                return (rc);
            }
        }
    }

    /**
     * Generates a new Item for a found Token, links it to the last one found and enters it into "itemList".
     *
     * @param key the key
     * @param kc the kc
     * @return generated rc
     */
    public final int newEntry(final String key, final int kc) {
        Item item = new Item();
        KCRCType kcrc = new KCRCType();
        item.setKey(key);
        int lastRc = 1;
        if (itemArray[kc] == null) {
            kcrc.setKc((short) kc);
            kcrc.setRc((short) 1);
            item.setLast(null);
            item.setContent(kcrc);
            itemArray[kc] = item;
        } else {
            kcrc.setKc((short) kc);
            lastRc = 1 + ((KCRCType) itemArray[kc].getContent()).getRc();
            kcrc.setRc((short) lastRc);
            item.setLast(itemArray[kc]);
            itemArray[kc] = item;
            item.setContent(kcrc);
        }
        difTokenCount++;
        return lastRc;
    }

    /**
     *
     * @return the last found KC
     */
    public final int getLastFoundKc() {
        return lastFoundKc;
    }

    /**
     * This method returns the hash value of a given token to enter it into the corresponding tree.
     *
     * @param key the key to be hashed
     * @return the hash value
     */
    public final int hash(final String key) {
        /*
         * int h,g;
         * h=0;
         * for(int i=0;i<key.length();i++){
         * h=(h << 4)+(int)key.charAt(i);
         * g=h & 0xF000;
         * if(g!=0){
         * g=g >> 7;
         * h=h ^ g;
         * }
         * }
         * return Math.abs(h%maxIdHash);
         */
        int temp = 0, i = 0;
        int len = key.length();
        while (i < len) {
            temp = ((temp << 4) + key.charAt(i)) % maxIdHash;
            i++;
        }
        return temp;
    }

    /**
     * @return the tree
     */
    public Tree getTree() {
        return tree;
    }

    /**
     * @return the hashArray
     */
    public Tree[] getHashArray() {
        return hashArray;
    }
    
    /**
     * searches the scantree for a Token-object by the KC and RC of the token
     * @param kc
     * @param rc
     * @return token object if found, null otherwise.
     */
    public Item findTokenByKcRc(final int kc, final int rc){
        if (this.itemArray[kc] == null){
            return null;
        }
        Item current = itemArray[kc];
        while (current != null) {
            if (((KCRCType) current.getContent()).getRc() == rc) {
                return current;
            }
            current = current.getLast();
        }
        return null;
    }
}
