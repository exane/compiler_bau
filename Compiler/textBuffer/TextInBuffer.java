/**
 * the textbuffer takes in a string, traverses it and produces
 * the contained chars one-by-one to the scanner, so that they can
 * be analysed and combined to tokens.
 */
package textBuffer;

/**
 * a buffer that traverses a given String char-by-char
 * and offers the option to set and return to retraction points.
 *
 * @author (Achim Guldner)
 * @version (1.0)
 */
public class TextInBuffer {

    /**
     * the text.
     */
    private String text;
    /**
     * the length of the text.
     */
    private int length;
    /**
     * the current position.
     */
    private int currentPos;
    /**
     * the retraction point.
     */
    private int retractionPoint;
    /**
     * true if the end is reached.
     */
    private boolean end = false;

    /**
     * the constructor initializes the buffer with a given string.
     *
     * @param text the text
     */
    public TextInBuffer(final char[] text) {
        this.text = new String(text);
        this.text += "\n" + (char) 26;
        this.length = this.text.length();
        currentPos = -1;
    }

    /**
     * @return next char
     */
    public final char getNextChar() {
        currentPos++;
        if (currentPos == (length - 2)) {
            end = true;
        }
        return (text.charAt(currentPos));
    }

    /**
     *
     * @return the length
     */
    public final int getLength() {
        return length;
    }

    /**
     * make current position the current retraction point.
     */
    public final void setRetractionPoint() {
        this.retractionPoint = this.currentPos - 1;
    }

    /**
     * return to last retraction point.
     */
    public final void returnToRetractionPoint() {
        this.currentPos = this.retractionPoint;
    }

    /**
     * @return true if end is reached
     */
    public final boolean end() {
        return this.end;
    }

}
