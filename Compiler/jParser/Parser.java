package jParser;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;

import POCO_generated.ParseTabs;
import POCO_generated.SysInfo;

import parseTree.ParseTreeBuilder;

import jScanner.KCRCType;
import jScanner.Scanner;


/**
 * jParser.Parser - A translation of PParser into Java
 * The file PParser as well as the POCO (TM)
 * Compiler generating System are (c) Michael J Eulenstein,2003-2006)
 * The class structure is based upon Ronald Mak (1996)
 *
 * The parser class is the core of the Parser.
 *
 * @author (Achim Guldner)
 * @author (Michael J Eulenstein)
 * @version (1.0)
 */
public class Parser {
    /**if this final variable is set to true,
     * a log file will be created and returned by the parse-method.*/
    private final boolean CREATELOGFILE = SysInfo.CREATELOGFILE;
    /**
     * if the final variable "PDEBUG" in SysInfo is set to true,
     * the parser will output the current stackpointer, action and state or rule
     * to the user while parsing.
     */
    private final boolean DEBUG = SysInfo.PDEBUG;
    //for reading the source code file
    /**The input stream reader for reading the source code file.*/
    private InputStreamReader isr;
    /**The File input stream for reading the source code file.*/
    private FileInputStream fis;
    /**the source code.*/
    private char[] sourceCode;
    /** the filename of the source code file*/
    public static String filename;
    //for parsing:
    /**
     * the maximum length of the parse stack.
     */
    private final int MAXPARSESTACKLENGTH = 512;
    /**the parse stack.*/
    private int[] parseStack;
    /**
     * the stack "pointer".
     */
    private int stackPointer;
    /**the lexer.*/
    public Scanner scanner;
    /**
     * The KCode representing EOF.
     */
    private final int endMedium = 0;
    /**
     * the current nonterminal symbol.
     */
    private int curNonT;
    /**
     * the length of the current rule.
     */
    private int curLength;
    /**
     * the current token.
     */
    private KCRCType kcrc;
    /**
     * true = the parser is currently reading.
     */
    private boolean read;
    /**
     * tells the parser when to stop.
     */
    private boolean done;
    /**
     * will be false if there was an error detected.
     */
    private boolean okay;
    /**
     * the current action.
     */
    private Action curAction;
    /**
     * the current Rule.
     */
    private int curRule;
    /**
     * the current state.
     */
    private int curState;
    /**
     * the last state.
     */
    private int oldState;
    /**
     * the current kc.
     */
    private int kc;
    /**
     * the current rc.
     */
    private int rc;
    /**
     * This enumeration represents all available actions.
     */
    private enum Action {
        shift, reduce, shiftred, accept, error, errorskip;
    }
    /**
     * The ParseTree.
     */
    private ParseTreeBuilder parseTreeBuilder;
    /**
     * The default constructor.
     */
    public Parser() {
        parseStack = new int[MAXPARSESTACKLENGTH];
        parseTreeBuilder=new ParseTreeBuilder();
    }

    /**
     * This method opens a source code file given to it through its filename,
     * generates a new TextInBuffer to handle it and hands the source code to this buffer.
     *
     * @see textBuffer.TextInBuffer
     *
     * @param fileName the file name
     * @throws IOException if the source code filename was wrong
     */
    public final void openFile(final String fileName) throws IOException {
        Parser.filename = fileName;
        int fileLength = (int) (new File(fileName).length());
        fis = new FileInputStream(fileName);
        isr = new InputStreamReader(fis);
        sourceCode = new char[fileLength];
        isr.read(sourceCode);
        isr.close();
        fis.close();
        this.scanner = new Scanner(sourceCode); //Create the scanner object
    }

    /**
     * This Method calls the Scanner.scanNextToken() for each found token
     * then parses the tokens.
     *
     * @return a log file if CREATELOGFILE==true, a string==null if the scanning process fails
     * and an empty String if logging is turned off.
     */
    public final String parse() {
        int tokenCount = 0;
        String logString = new String("source-code file: " + Parser.filename + "\nAnalysis yielded the following results:");
        if (!CREATELOGFILE) {
            logString = "";
        }
        if (scanner.getTib() == null) {
            return ("no sourcefile was loaded");
        }
        long start = new Date().getTime(); //start time measurement!
        //initialization
        kcrc = new KCRCType(); // a token buffer
        done = false; // true when the parser finishes
        okay = true; // while this is true, no error has occurred
        read = true; // currently reading tokens
        curState = 0; // the current state of the parser PDA
        stackPointer = 0; //the stack pointer of the parser PDA
        parseStack[0] = curState; // the parse stack
        do { // while not done
            if (read) { //perform scan:
              System.out.println("\n\033[96m[ Reading token ]\033[0m");
                kcrc = scanner.scanNextToken(); //retrieve next token from scanner
              System.out.println("  \033[92m[ Done ]\033[0m\n");
                if (kcrc == null) { // the scanner encountered an error
                  System.out.println("*** Scanner error!");
                    logString = null;
                    okay = false;
                    done = true;
                    break;
                } //otherwise:
                tokenCount++;
                this.kc = kcrc.getKc();
                this.rc = kcrc.getRc();
                } else if (CREATELOGFILE) {
                    logString += "\n" + kcrc.toString();
            }
            //parse:
            read = true;
            this.performAction(); // accept, push or reduce
            if (CREATELOGFILE) {
                logString += "\n" + actionToString();
            }
            if (curAction == Action.error) {
                if (CREATELOGFILE) {
                    logString += "\n";
                }
                logString += "***A parse-error occured***\n at " +
                             "Stack-position: " + stackPointer + "#" + oldState +
                             " Action: " + actionToString() + "CC=" + kc +
                             " RC=" + rc + "\n" + dumpStack();
                okay = false;
                done = true;
            }
        } while (!done);
        System.out.println("++->Done");
        long compileTime = 0;
        long generateTime = 0;
        if(this.okay) {
            if(POCO_generated.SysInfo.PRINTPARSETREE) {
                System.out.println(" +->Tree-nodes:");
                this.parseTreeBuilder.printVerboseTree();
            }
            if (POCO_generated.SysInfo.PRINTOBJECTCODE)
                System.out.println(" |\n +->Generating object code...\n");
            compileTime = new Date().getTime() - start;
            long restart = new Date().getTime();
            this.parseTreeBuilder.generate(); //TRIGGER CODE GENERATION.
            generateTime = new Date().getTime() - restart;
        }
        // Print results--------
        System.out.println("|\n+-->Parser results:");
        if (compileTime > 1000) {
            System.out.println("+-->Compile-time: " +
                    compileTime / 1000 + " seconds");
        } else {
            System.out.println("+-->Compile-time: " +
                    compileTime + " milliseconds");
        }
        if (generateTime > 1000) {
            System.out.println("+-->Generation-time (including possible assembly time): " +
                    generateTime / 1000 + " seconds");
        } else {
            System.out.println("+-->Generation-time (including possible assembly time): " +
                    generateTime + " milliseconds");
        }
        System.out.println("+-->there were " + tokenCount + " tokens scanned");
        System.out.println("+-->" + scanner.getDifTokenCount() + " of which were entered into the scanner-tree");
        if (!okay) {
            System.out.println("++->The parser encountered an error.\n" +
            		           " +->For more information consult the log-file");
        } else {
            System.out.println("+-->Source-code was accepted.");
        }
        return logString;
        // ---------
    }

    /**
     * this method performs the current action.
     */
    private void performAction() {
        int ops;
        oldState = curState;
        ops = getAction(); //determine Action from current status, content in stack and read token
        if (ops < 0) {
            curAction = Action.shift; //shift is detected:
          curState = Math.abs(ops);// - 4;
            if(okay){
                parseTreeBuilder.shift(kc, rc, scanner.findTokenByKCRC(kc, rc)); //trigger shift method in parsetree builder
            }
            push(); //push to stack
        } else if (ops == 0) {
            curAction = Action.accept; //accept is detected
            done = true; //done parsing
            okay = okay & (kc == endMedium) & (stackPointer == 1); // check if no error occurred so far & the current object is the axion of the grammar & there is only one object left in the stack! 
          if(!okay) System.out.println("*** Accept-failure. (Stack-failure)");
        } else if (ops <= ParseTabs.MAXREGEL) {
            curAction = Action.reduce; //reduce is detected
            curRule = ops;
            parseTreeBuilder.reduce(curRule); //trigger reduce method in parsetree builder
            read = false;
            pop(); //pop from parseTree
        } else if (ops == ParseTabs.MAXREGEL1) {
            curAction = Action.error; // error occurred
        } else if (ops <= ParseTabs.MAXREGEL2) {
            curAction = Action.shiftred; //shiftred detected
            curRule = ops - ParseTabs.MAXREGEL1 - 1; // BUG: Of by one. ( -2 before )
          System.out.println("++++> Shift,Reduce: " + (curRule + 0));
            if(okay){
                parseTreeBuilder.shift(kc, rc, scanner.findTokenByKCRC(kc, rc)); //first shift...
                parseTreeBuilder.reduce(curRule); //then reduce
            }
            stackPointer++;
            pop();
        }
    }

    /**
     * After poping the elements from the stack this method is called.
     * it calculates the resulting entity and then pushes it back to the stack.
     */
    private void go() {
        final int anyNT = -1;
        int index, val;
        index = ParseTabs.GBase[curNonT];
        if (index < 0) {
            curState = Math.abs(index);
        } else {
            do {
                val = ParseTabs.GotoNT[index];
                index++;
            } while ((val != curState) && (val != anyNT));
            curState = ParseTabs.GotoNxt[index - 1];
        }
        push();
    }

    /**
     * push to parseStack.
     */
    private void push() {
        stackPointer++;
        if (DEBUG) {
            System.out.println("+-->ParseStack-Push called. Stackpointer = " + stackPointer); //DEBUG
        }
        if (stackPointer > MAXPARSESTACKLENGTH) {
            System.out.println("+->***Parse stack overflow!***");
            System.exit(0);
        }
        parseStack[stackPointer] = curState;
    }

    /**
     * pop from parsestack.
     */
    private void pop() {
        curNonT = ParseTabs.RuleInfo[curRule] % 1024;
        curLength = ParseTabs.RuleInfo[curRule] / 1024;
        if (DEBUG) {
            if (curLength!=0)
                System.out.print("+-->ParseStack-Pop called. Stackpointer reduced from " + stackPointer);
            else
                System.out.println("+-->ParseStack-Pop called. However the stackpointer remains at " + stackPointer);
        }
      System.out.print(" (by " + curLength + " because of " + curRule + ")");
        stackPointer -= curLength;
        if (DEBUG) {
            if (curLength!=0)
            System.out.println(" to " + stackPointer); //DEBUG
        }
        if (stackPointer < 0) {
          System.out.println("*** stackPointer-failure: " + stackPointer);
            okay = false;
            done = true;
        } else {
            curState = parseStack[stackPointer];
        }
        go();
    }
    /**
     * This method looks up the next action in parse POCO_generated,
     * that corresponds to the current state and found token.
     *
     * @return the found action
     */
    private int getAction() {
        int index, ops, val;

        index = ParseTabs.ABase[curState];
        if (index < 0) {
            ops = index * (-1);
        } else {
            do {
                val = ParseTabs.AktKC[index];
                ops = ParseTabs.AktOP[index];
                index++;
            } while((val != kc) && (val != ParseTabs.ANYKC));
        } System.out.println("\033[95m~~> Action: " + ops + ", because: " + curState + " (kc:" + kc + ", index:" + index + ")\033[0m");
        return ops;
    }

    /**
     * this method creates a string that represents the current action.
     * @return the current action (curAction) as string
     */
    private String actionToString() {
        String s;
        switch(curAction) {
            case shift: {
                s = "Shift    " + curState;
                break;
            }
            case accept: {
                s = "Accept.  ";
                break;
            }
            case reduce: {
                s = "Reduce   " + curRule;
                break;
            }
            case shiftred: {
                s = "ShiftReduce " + curState;
                break;
            }
            case error: {
                s = "Error.   ";
                break;
            }
            default: {
                s = "<<<ERROR>>>";
                break;
            }
        }
        return s;
    }

    /**
     * returns all Objects currently entered in the stack
     * (mostly for debugging).
     *
     * @return the objects currently in the stack. The function calls the
     * "toString" method for all objects and enters them into a string then
     * returns this string.
     */
    public final String dumpStack() {
        String s = "momentarily in stack:\n";
        if (stackPointer <= 0) {
            s += "nothing!";
            return s;
        }
        for (int i = 0; i < stackPointer; i++) {
            s += parseStack[i] + " ";
        }
        return s;
    }
}  
