  
/********************************************************************/
/* GENERATED BY P O C O - Compiler Generator V8.14*DOS* 20-Sep-2014 */
/* CopyRight POCO (c) Dr Michael J Eulenstein 1984, 1987, 1991,2003.*/
/* Grammar File  : TINY05C.GRM                    Options: 19D3000A */
/* GENERATION DATE / TIME : Mo 29. Dez 2014/20:50:23                */
/********************************************************************/
  
package POCO_generated;
  
public class ParseTabs {
  
     public static int[] GotoNT = {
  
          16, 17, 22, 42, 63, 74, -1, 26, 64, 66, -1, 23, 43, -1, 65, -1,  
          24, 31, 34, 35, 36, 47, 62, 75, -1, 44, -1, 45, -1,0 };
  
     public static int[] GotoNxt = {
  
          32, 33, 37, 54, 70, 76,  2, 41, 71, 73,  3, 38, 55, 28, 72, 60,  
          39, 46, 49, 50, 51, 58, 69, 77, 27, 56, 29, 57, 30,0 };
  
     public static short[] AktKC = {
  
            1,  10, 102, 103, 105, 106, 107, 108, 110, 112, 113, 127,   0,  
          127,   5, 127,   1, 123, 124, 127, 104, 127, 104, 127,  96, 127,  
            1, 127,   1, 127, 116, 127, 118, 119, 127, 117, 127,   5, 109,  
          127,   5, 114, 127,   5, 114, 127, 111, 118, 127, 101, 127, 122,  
          127, 106, 127, 124, 127, 100, 127,   5, 114, 115, 127, 118, 127,  
          117, 127,   5, 114, 115, 127, 120, 121, 127, 124, 127, 106, 127,  
           99, 127,   5, 114, 127, 114, 127,   5, 114, 127, 106, 127,0 };
  
     public static int[] AktOP = {
  
          -20, -15, -25, -19, -18, -17, -24, -21, -16, -23, -22,  47,   0,  
           47, -26,   1,  79, -31,  78,  47, -34,  16, -35,  18, -36,  47,  
           69,  47, -40,  47, -42,  47, -44, -43,  24, -45,  26, -26, -47,  
           47, -26, -48,  47, -26,  52,  47, -52, -44,  47, -53,  47,  77,  
           47,  63,  47,  54,  47,  81,  34, -26,  60, -63,  47, -44,  23,  
          -45,  25, -65,  55, -64,  47, -66, -67,  47,  53,  47, -74,  47,  
          -75,  47, -26,  61,  47,  56,  47, -26, -78,  47,  83,  47,0 };
  
     public static int[] ABase = {
  
              0,    12,    14,    -3,   -36,   -37,   -38,   -39,   -40,  
            -41,   -42,   -43,   -44,   -45,   -46,    16,     0,     0,  
             20,    22,    24,    26,     0,    16,    16,    28,     0,  
             30,    32,    35,   -28,    16,    37,    40,    16,    16,  
             16,    43,    46,   -22,    49,    -2,     0,    16,    16,  
             16,    51,    16,    53,   -17,   -19,   -20,    55,    57,  
             59,    63,    65,   -27,   -14,    67,   -10,    71,    16,  
              0,     0,    55,     0,    74,    76,    78,    80,    83,  
             -9,   -11,     0,    16,    85,   -32,    88,0 };
  
     public static int[] GBase = {
  
              0,    -1,     0,     7,   -11,   -61,   -12,    11,   -59,  
             14,    -4,    16,    -5,    -6,    -7,    -8,    -9,   -10,  
            -13,    25,    27,   -68,   -62,   -14,0 };
  
     public static int[] RuleInfo = {
  
           1024,  1025,  3074,  1026,  3076,  3077,  1029,  5126,  7174,  
           3080,  1032,  3081,  5130,  7178,  4108,  4109,  1038,  3086,  
           1039,  3087,  3088,  2065,  2066,  3083,  1035,  3079,  1031,  
           3091,  1043,  3092,  1044,  1044,  3093,  1046,    22,  9239,  
           1027,  1027,  1027,  1027,  1027,  1027,  1027,  1027,  1027,  
           1027,  1027,0 };
  
    public static final int MAXREGEL = 46;
    public static final int MAXREGEL1 = 47;
    public static final int MAXREGEL2 = 94;
    public static final int ERRORSYMB = 0;
    public static final int ANYKC = 127;
  
} /* ParsTabs */
  
