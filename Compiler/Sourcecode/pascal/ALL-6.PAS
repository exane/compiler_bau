Program Test;

function LeapYear (Y: Integer) : Boolean;
begin
  LeapYear := ((Y mod 400) = 0) or ((Y mod 100 <> 0) and (Y mod 4 = 0));
end (* LeapYear *);

begin
end.

