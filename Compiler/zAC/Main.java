/**
 * 
 */
package zAC;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;

import parseTree.ParseTreeNode;

import java.io.IOException;

import POCO_generated.SysInfo;

import jParser.Parser;


/**
 * The main class of the Compiler that represents the UI
 * i.e. asks him/her for a sourcecode-file
 * and calls the "Parser.parse()" method
 * (see below) that performs the parsing.
 *
 * @see jParser.Parser#parse
 *
 * @author (Achim Guldner)
 * @version (1.0)
 */
public class Main {
    /**the parser class.*/
    private static Parser parser = new Parser();
    /**the reader for user entries.*/
    private static BufferedReader keyboard =
        new BufferedReader(new InputStreamReader(System.in));
    /**the filename of the source code file.*/
    public static String scFilename = new String("sourcecode/tiny/tiny-001");
    /**the file output stream for saving the log file.*/
    private static FileOutputStream fos;

    /**
     * The main method asks the user for the sourcecode-file to be scanned,
     * reads the given file and passes it to the scanner.
     * Furthermore it catches ioexceptions and notifies the user
     *  if files cannot be found.
     *
     * @param args arguments may be handed in when starting the method
     * (none used)
     */
    public static void main(final String[] args) {
        System.out.print("" +
        		"+--------------------------------------------------+\n" +
                "|      W  E  L  C  O  M  E    t o    z A C !!      |\n" +
                "|Please enter the location of the source code file:|\n" +
                "|(without the file-extension " + SysInfo.SourceFileSuffix + ")                 |\n" +
                "| e.g. sourcecode/xpl/ALL-1)                       |\n" +
                "+--------------------------------------------------+\n" +
                "+--> Used grammar file: \"" + POCO_generated.SysInfo.grmFileName + "\"\n|\n" +
                "+--< ");
        try { //Read in source code filename
            String buffer = keyboard.readLine();
            if (!buffer.equals(""))
                scFilename = buffer + SysInfo.SourceFileSuffix;
            else
                scFilename += SysInfo.SourceFileSuffix;
        } catch (IOException ioe) {
            System.out.println("+->*IO error***\n+-->" + ioe.getMessage());
        }
        try { 
            System.out.println("|");
            parser.openFile(scFilename); //hand over the source code filename to the parser for it to open
            System.out.println("++->found \"" + scFilename + "\"");
            System.out.println(" +->Analyzing...\n");
            if (SysInfo.PDEBUG || (SysInfo.PTDEBUG == 1) || SysInfo.SDEBUG)
                System.out.println("+-->DEBUG Mode!");
            String log  = parser.parse(); // START PARSER => START COMPILER
            File f = new File("Sourcecode/list.txt"); //save log-file and output possible error messages
            if (!f.exists()) {
                try {
                    f.createNewFile();
                } catch (IOException ioe) {
                    System.out.println("+->*Could not create file \"sourcecode/list.txt\"***\n+-->"+ ioe.getMessage());
                }
            }
            if (log != null) {
                if (!log.equals("")) {
                    System.out.println("+-->Log-file saved in:\"Sourcecode/list.txt\"");
                    
                    fos = new FileOutputStream("Sourcecode/list.txt");
                    fos.write(log.getBytes());
                    fos.close();
                } else {
                    System.out.println("+-->No log-file has been created");
                }
            } else {
                fos = new FileOutputStream("Sourcecode/list.txt");
                fos.write(("error parsing source-code file:\n\"" + scFilename + "\"").getBytes());
                fos.close();
                System.out.println("+-->No log file has been created, because a parse-error was detected.");
            }
        } catch (IOException ioe) {
            System.out.println("+->*File not found***\n+-->"+ ioe.getMessage());
        }

    }
}
