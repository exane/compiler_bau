/**
 * an implementation of a red-black 234 tree taken from robert sedgewicks book
 * "Algorithmen in Java 1-4" for creating lexer trees.
 * The Item-class represents the content and key of a tree node,
 * the ParseTreeNode-class is one element within a tree and the Tree-class describes the
 * tree-data structure.
 */
package red_black_tree;

/**
 * This implementation of a 234-red-black-tree was taken from the book<br/>
 * Sedgewick, Robert: Algorithmen in Java: Teil 1-4.<br/>
 * 3.,überarbeitete Auflage, Pearson Education, Inc, München: Pearson Studium<br/>
 * 2003 - ISBN: 978-3-8273-7072-3 P. 579, 622f.
 *
 * @author (Robert Sedgewick)
 * @author (Achim Guldner)
 * @version (1.0)
 */
public class Tree {

    /**
     * a ParseTreeNode inside a red-black-tree can be either red or black.
     * these variables describe this state.
     */
    private static final boolean BLACK = false;
    /**
     * a ParseTreeNode inside a red-black-tree can be either red or black.
     * these variables describe this state.
     */
    private static final boolean RED = true;
    /**
     * the tree is defined through only one node.
     * The root.
     */
    private Node root;

    /**
     * the default constructor.
     */
    public Tree() { }

    /**
     * Insert Item x into the tree.
     *
     * @param x the item to be inserted
     */
    public final void insert(final Item x) {
        root = insertRecursive(root, x, BLACK);
        root.color = BLACK;
    }

    /**
     * Search for a key in the tree.
     *
     * @param key the key to find
     * @return the found item or null if the key is not in the tree.
     */
    public final Item search(final String key) {
        return searchRecursive(root, key);
    }

    /**
     * The recursive function that is called when searching.
     *
     * @param h the node where to start the recursive search
     * @param key the key to find
     * @return found item or null if not found
     */
    private Item searchRecursive(final Node h, final String key) {
        if (h == null) { //the searched key was not found
            return null;
        }
        if (key.compareToIgnoreCase(h.content.key) == 0) { //the key was found
            return h.content;
        }
        if (h.content.key.compareToIgnoreCase(key) > 0) { //the key is less than the current node. therefore we need to search in the left sub-tree
            return searchRecursive(h.left, key);
        } else {  //the key is greater than the current node. therefore we need to search in the right sub-tree
            return searchRecursive(h.right, key);
        }
    }

    /**
     * tells the caller if a specified node is red.
     *
     * @param x the node
     * @return true if a node is red
     */
    private boolean red(final Node x) {
        if (x == null) {
            return false;
        }
        return x.color;
    }

    /**
     * recursive function that is called when an item is to be inserted.
     *
     * @param h the node where to start the insertion process recursively
     * @param x the item to insert
     * @param sw is the path red or black?
     * @return the inserted node
     */
    private Node insertRecursive(Node h, final Item x, final boolean sw) {
        if (h == null) { //if an end is reached, the node can be inserted.
            return (new Node(x, RED));
        }
        if (red(h.left) && red(h.right)) { //this if statement rearranges the colors in the tree.
            h.color = RED;
            h.left.color = BLACK;
            h.right.color = BLACK;
        }
        if (x.less(h.content)) { //left recursion
            h.left = insertRecursive(h.left, x, BLACK);
            if (red(h) && red(h.left) && sw) {
                h = rotateRight(h);
            }
            if (red(h.left) && red(h.left.left)) {
                h = rotateRight(h);
                h.color = BLACK;
                h.right.color = RED;
            }
        } else { //right recursion
            h.right = insertRecursive(h.right, x, RED);
            if (red(h) && red(h.right) && !sw) {
                h = rotateLeft(h);
            }
            if (red(h.right) && red(h.right.right)) {
                h = rotateLeft(h);
                h.color = BLACK;
                h.left.color = RED;
            }
        }
        return h;
    }

    /**
     * rotates right with h as pivot node.
     *
     * @param h the pivot node
     * @return the new root of the subtree
     */
    private Node rotateRight(final Node h) {
        Node x = h.left;
        h.left = x.right;
        x.right = h;
        return x;
    }

    /**
     * rotates left with h as pivot node.
     *
     * @param h the pivot node
     * @return the new root of the subtree
     */
    private Node rotateLeft(final Node h) {
        Node x = h.right;
        h.right = x.left;
        x.left = h;
        return x;
    }

}
