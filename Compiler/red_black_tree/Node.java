package red_black_tree;

/**
 * one node of the tree.
 * 
 * @author (Achim Guldner)
 * @version (1.0)
 */
public class Node {

    /**
     * the left son.
     */
    protected Node left;
    /**
     * the right son.
     */
    protected Node right;
    /**
     * this node's color.
     */
    boolean color;
    /**
     * the content.
     */
    protected Item content;

    /**
     * The default constructor.
     */
    Node() { }

    /**
     * @param item the item to enter
     * @param color the node's color
     */
    Node(final Item item, final boolean color) {
        this.color = color;
        this.content = item;
    }
}
