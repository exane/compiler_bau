package red_black_tree;

public class TreeTest{
    public static Item itemlist[];
    public static void main(String[] args) {
        Tree t=new Tree();
        itemlist=new Item[20];
        System.out.println("Inserting:");
        for(int i=0;i<20;i++){
            Item it=new Item();
            it.key=(""+(char)(i+40));
            itemlist[i]=it;
            t.insert(it);
            System.out.print(it.key);
        }
        System.out.println();
        System.out.print("Finding: "+"42"+"... ");
        Item it=t.search("42");
        if(it!=null)
            System.out.println("found: "+it.key);
        else
            System.out.println("NOT found!");
        System.out.print("Finding: "+"23"+"... ");
        it=t.search("23");
        if(it!=null)
            System.out.println("found: "+it.key);
        else
            System.out.println("NOT found!");
        System.out.print("Finding: "+"foofoo"+"... ");
        it=t.search("foofoo");
        if(it!=null)
            System.out.println("found: "+it.key);
        else
            System.out.println("NOT found!");
        for(int i=19;i>=0;i--){
            System.out.print("Finding: "+itemlist[i].key+"... ");
            it=t.search(itemlist[i].key);
            if(it!=null)
                System.out.println("found: "+it.key);
            else
                System.out.println("NOT found!");
            if((i % 3)==0){
                System.out.print("Finding: "+(char)(i+25)+"... ");
                it=t.search(""+(char)(i+25));
                if(it!=null)
                    System.out.println("found: "+it.key);
                else
                    System.out.println("NOT found!");
            }
                
        }
    }
}
