package red_black_tree;

import POCO_generated.SysInfo;

/**
 * This is a reusable class representing the key and content of a tree node.
 * Other than getters and setters it contains the "last item" and a method that
 * compares this item's key to another item's
 *
 * @author (Achim Guldner)
 * @version (1.0)
 */
public class Item {
    private static final boolean CASESENSITIVE = SysInfo.CASESENSITIVE;
    /**
     * the key.
     */
    protected String key = null;
    /**
     * the content.
     */
    protected Object content;
    /**
     * The "last" item can be any logical item inside the tree it is NOT the predecessor.
     * But can be chosen individually by the user as a static reference e.g. to the last inserted item of a kind
     */
    protected Item last;

    /**
     * the default constructor.
     */
    public Item() { }

    /**
     * @param x the item to compare with
     * @return true if this key is less than the key of item x
     */
    public final boolean less(final Item x) {
        if (CASESENSITIVE)
            return ((this.key.compareTo(x.key) < 0) ? true : false);
        else
        return ((this.key.compareToIgnoreCase(x.key) < 0) ? true : false);
    }

    /**
     * @return the key
     */
    public final String getKey() {
        return key;
    }

    /**
     * @param key the key
     */
    public final void setKey(final String key) {
        this.key = key;
    }

    /**
     * @return the content
     */
    public final Object getContent() {
        return content;
    }

    /**
     * @param content the content
     */
    public final void setContent(final Object content) {
        this.content = content;
    }

    /**
     * @return the last item
     */
    public final Item getLast() {
        return last;
    }

    /**
     * @param last the last item
     */
    public final void setLast(final Item last) {
        this.last = last;
    }
}
