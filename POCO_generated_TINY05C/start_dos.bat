::start_dos
@ECHO OFF
IF NOT "%1" == "" GOTO START

ECHO S->Welcome to zAC for Dos and Windows
ECHO.
ECHO S->Usage: Enter start_dos <location of the Compiler>
ECHO.
ECHO S->(like "start_dos C:\Compiler"):
ECHO.


GOTO END

:START

ECHO S->Welcome to zAC for Dos and Windows
ECHO.

IF NOT EXIST %1/zAC/Main.class GOTO ERROR1
IF NOT EXIST %1/jParser/Parser$Action.class GOTO ERROR1
IF NOT EXIST %1/jParser/Parser.class GOTO ERROR1
IF NOT EXIST %1/jScanner/ScanTreeBuilder.class GOTO ERROR1
IF NOT EXIST %1/jScanner/KCRCType.class GOTO ERROR1
IF NOT EXIST %1/jScanner/ScanTreeBuilder.class GOTO ERROR1
IF NOT EXIST %1/red_black_tree/Item.class GOTO ERROR1
IF NOT EXIST %1/red_black_tree/Node.class GOTO ERROR1
IF NOT EXIST %1/red_black_tree/Tree.class GOTO ERROR1
IF NOT EXIST %1//textBuffer/TextInBuffer.class GOTO ERROR1

ECHO S->exit any time by pressing ctrl+C
ECHO.
ECHO S->Copying and Compiling Files

::XCOPY blo.txt %1

XCOPY ParseTabs.java %1\POCO_generated
XCOPY ParseTreeBuilder.java %1\parseTree
XCOPY ParseTreeNode.java %1\parseTree
XCOPY ScanTabs.java %1\POCO_generated
XCOPY SysInfo.java %1\POCO_generated
cd %1

javac parseTree/ParseTreeBuilder.java
javac parseTree/ParseTreeNode.java
javac POCO_generated/ScanTabs.java
javac POCO_generated/SysInfo.java
javac POCO_generated/ParseTabs.java
javac jParser/Parser.java
javac zAC/Main.java

GOTO END

:ERROR1
ECHO S->Compilern was not found at given location
ECHO.

:END

@ECHO ON



